import logging
from typing import Any, Dict
from app import cfg as config

app_config = config.get_config()
logging.basicConfig(
    handlers=config.logging_handler(),
    format=config.get_logger_format(),
    level=config.get_logging_type().get(app_config["logging_type"], "INFO"),
)


class Logging:
    @staticmethod
    def get_logger():
        return logging

    @staticmethod
    def formatting_message(msg: Any) -> Dict[str, Any]:
        """formatting message to get better logs
        :param msg: message that want to print
        :return: dictionary message
        """
        return msg

    @staticmethod
    def info(message: Any):
        """Print logging info message
        :param message: any message
        :return: None
        """
        logging.info(Logging.formatting_message(message))

    @staticmethod
    def debug(message: Any):
        """Print logging debug message
        :param message: any message
        :return: None
        """
        logging.debug(Logging.formatting_message(message))

    @staticmethod
    def error(message: Any):
        """Print logging error message
        :param message: any message
        :return: None
        """
        logging.error(Logging.formatting_message(message))

    @staticmethod
    def warning(message: Any):
        """Print logging warning message
        :param message: any message
        :return: None
        """
        logging.warning(Logging.formatting_message(message))

    @staticmethod
    def exception(message: Any):
        """Print logging exception() message
        :param message: any message
        :return: None
        """
        logging.exception(Logging.formatting_message(message))
