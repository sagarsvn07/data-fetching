import os
from datetime import datetime, timedelta
import time
from typing import Any
import io

import pandas as pd
from pandas import DataFrame

from app import cfg
from utils import decode_string, Logging
from services import MNCCloudServices
from utils.util import validate_vdb_schema


class MNCCloudVDBService:
    def __init__(self):
        self.today_date = datetime.now()
        self.yesterday_date = self.today_date - timedelta(days=1)
        self.upload_date_format = "{}{month:02d}{day:02d}".format(
            self.yesterday_date.year,
            # this format makes month 2 digits (eg: 5 -> 05, 6 -> 06)
            month=self.yesterday_date.month,
            day=self.yesterday_date.day,
        )
        self.username = decode_string(cfg.mnc_cloud_username)
        self.password = decode_string(cfg.mnc_cloud_password)
        self.services = MNCCloudServices.from_base_url(cfg.http_base_url)
        self.mnc_cloud_path_file = cfg.mnc_path_file

    def extract_all_csv_data(self, file_name: str, model: Any):
        created = pd.DataFrame()
        updated = pd.DataFrame()

        vdb_url = f"{self.username}/{self.mnc_cloud_path_file}"
        csv_list = self.services.fetch_list(vdb_url, self.username, self.password)
        all_created_list = [file for file in csv_list if
                            file.startswith(f"{file_name}")]
        if file_name == "content":
            all_created_list = [file for file in all_created_list if
                                file.startswith('content_') and not file.startswith(
                                    'content_core_') and not file.startswith(
                                    'content_core_synopsis') and not file.startswith('content_having_')]

        if file_name == "homepage":
            all_created_list = [file for file in all_created_list if
                                file.startswith('homepage') and not file.startswith('homepage_having_content')]
        for csv_file in all_created_list:
            if csv_file.endswith("created.csv"):
                file_url = f"{self.username}/{self.mnc_cloud_path_file}/{csv_file}"
                all_created = self.services.fetch_csv_data(file_url, self.username, self.password)
                if len(all_created) == 0:
                    time.sleep(5)
                    self.services.delete_file_after_read(url=file_url, username=self.username,
                                                         password=self.password)
                else:
                    time.sleep(3)
                    if file_name == "customer":
                        all_created = all_created.fillna("nan")
                    valid_created = validate_vdb_schema(data=all_created, url=file_url, model=model)
                    created = pd.concat([created, valid_created])

            elif csv_file.endswith("updated.csv"):
                file_url = f"{self.username}/{self.mnc_cloud_path_file}/{csv_file}"
                all_updated = self.services.fetch_csv_data(file_url, self.username, self.password)
                if len(all_updated) == 0:
                    time.sleep(3)
                    self.services.delete_file_after_read(url=file_url, username=self.username,
                                                         password=self.password)
                else:
                    time.sleep(3)
                    if file_name == "customer":
                        all_updated = all_updated.fillna("nan")
                    valid_updated = validate_vdb_schema(data=all_updated, url=file_url, model=model)
                    updated = pd.concat([updated, valid_updated])

        return created, updated

    def extract_pay_tv_provider(self, file_name: str, model: Any):
        data = pd.DataFrame()
        vdb_url = f"{self.username}/{self.mnc_cloud_path_file}"
        csv_list = self.services.fetch_list(vdb_url, self.username, self.password)
        all_csv_list = [file for file in csv_list if file.startswith(f"{file_name}")]
        for csv in all_csv_list:
            file_url = f"{self.username}/{self.mnc_cloud_path_file}/{csv}"
            csv_file = self.services.fetch_csv_data(file_url, self.username, self.password)
            if len(csv_file) == 0:
                time.sleep(3)
                self.services.delete_file_after_read(url=file_url, username=self.username,
                                                     password=self.password)
            else:
                time.sleep(3)
                validate_csv = validate_vdb_schema(data=csv_file, url=file_url, model=model)
                data = pd.concat([data, validate_csv])
        data = data.drop_duplicates(ignore_index=True)
        return data

    def move_data_to_be_process(self, counter: int, created_data: DataFrame, file_name: str, mapping_file: bool = False):
        """:param counter: counter
        :param created_data: to_be_process_dataframe
        :param file_name: table name
        :return: upload into the MNC CLOUD
        """
        vdb_url = f"{self.username}/{self.mnc_cloud_path_file}/{file_name}/{file_name}_{self.upload_date_format}.csv"
        if not mapping_file:
            to_be_process_url = vdb_url.replace('.csv', '_chunk_{}.csv'.format(str(counter)))
        else:
            to_be_process_url = vdb_url
        bytes_created = io.BytesIO()
        bytes_created.seek(0)
        created_data.to_csv(bytes_created, index=False)
        try:
            time.sleep(3)
            self.services.upload_file(url=to_be_process_url,
                                      data=bytes_created.getvalue(),
                                      username=self.username,
                                      password=self.password
                                      )
            Logging.info(f"file is uploaded to MNC Cloud at {to_be_process_url}")
        except Exception as e:
            Logging.error(f"error while uplaoding to the data on MNC Cloud at {to_be_process_url}:{str(e)}")
        return to_be_process_url

    def delete_all_csv_data(self, file_name: str):

        vdb_url = f"{self.username}/{self.mnc_cloud_path_file}"
        time.sleep(3)
        csv_list = self.services.fetch_list(vdb_url, self.username, self.password)
        all_created_list = [file for file in csv_list if
                            file.startswith(f"{file_name}")]
        if file_name == "content":
            all_created_list = [file for file in all_created_list if
                                file.startswith('content_') and not file.startswith(
                                    'content_core_') and not file.startswith(
                                    'content_core_synopsis') and not file.startswith('content_having_')]

        if file_name == "homepage":
            all_created_list = [file for file in all_created_list if
                                file.startswith('homepage') and not file.startswith('homepage_having_content')]
        if file_name == "content_core":
            all_created_list = [file for file in all_created_list if
                                file.startswith('content_core') and not file.startswith('content_core_synopsis')]

        for csv_file in all_created_list:
            file_url = f"{self.username}/{self.mnc_cloud_path_file}/{csv_file}"
            all_created = self.services.fetch_csv_data(file_url, self.username, self.password)

            archived_url = f"{self.username}/vdb/tmp/{csv_file}"
            bytes_created = io.BytesIO()
            bytes_created.seek(0)
            all_created.to_csv(bytes_created, index=False)
            # change path and add tmp
            time.sleep(3)
            self.services.delete_and_move_data_after_read(
                tmp_url=archived_url,
                url=file_url,
                data=bytes_created.getvalue(),
                username=decode_string(cfg.mnc_cloud_username),
                password=decode_string(cfg.mnc_cloud_password),
            )
        return True

    def fetch_list_from_process_file(self, file_name: str):
        time.sleep(3)
        vdb_url = f"{self.username}/{self.mnc_cloud_path_file}/{file_name}"
        csv_list = self.services.fetch_list(vdb_url, self.username, self.password)
        return csv_list

    def fetch_mapping_file_csv(self, file_name: str,model:Any):
        time.sleep(3)
        vdb_url = f"{self.username}/{self.mnc_cloud_path_file}"
        file_list = self.services.fetch_list(vdb_url, self.username, self.password)
        if file_name == "content_core":
            required_list = [file for file in file_list if file.startswith(f"{file_name}") and not file.startswith('content_core_synopsis')]
        else:
            required_list = [file for file in file_list if file.startswith(f"{file_name}")]
        latest_file = self.extract_date(required_list)
        file_url = f"{self.username}/{self.mnc_cloud_path_file}/{latest_file}"
        csv_file = self.services.fetch_csv_data(file_url, self.username, self.password)
        validate_csv = validate_vdb_schema(data=csv_file, url=file_url, model=model)
        return validate_csv

    @staticmethod
    def extract_date(required_list):
        # Extract datetime from filenames and find the latest
        latest_filename = None
        latest_datetime = None
        for filename in required_list:
            # Extract datetime from the filename
            datetime_str = filename.split('_')[3].split('.')[0]
            if datetime_str == 'created':
                datetime_str = filename.split('_')[2].split('.')[0]
            file_datetime = datetime.strptime(datetime_str, "%Y%m%d")

            # Compare datetime with the latest recorded
            if latest_datetime is None or file_datetime > latest_datetime:
                latest_datetime = file_datetime
                latest_filename = filename

        return latest_filename




