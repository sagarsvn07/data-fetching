import base64
import hashlib
import logging
import boto3
import random
import string
from datetime import datetime
from typing import List, Dict, Any
from configs.config import AwsConfig
import pandas
from ingestor.common.read_write_from_s3 import ConnectS3


def hash_string(test: str) -> str:
    """Hash text as sha1 algorithm and return has string
    :param test: string plain text string
    :return:
    """
    return hashlib.sha1(test.encode("utf-8")).hexdigest()


def encode_string(text: str) -> str:
    """Hash plain string to base 64 text string
    :param text: string text
    :return: string encode text
    """
    return base64.b64encode(text.encode("utf-8")).decode("utf-8")


def decode_string(hash_str: str) -> str:
    """Un hash text string to plain text string
    :param hash_str: base 64 encode string
    :return: plain text string
    """
    return base64.b64decode(hash_str).decode("utf-8")


def generate_ud_key() -> str:
    """Generate random Ud key for mocking data
    :return: string key
    """
    return "".join(random.choice(string.digits) for _ in range(20))


def remove_duplicate_created_updated(
        created: pandas.DataFrame, updated: pandas.DataFrame, keys: List[str]
) -> pandas.DataFrame:
    """Merge created and updated value in one unique records
    :param keys: list of unique string that we want to merge
    :param created: created records dataframe
    :param updated: updated records dataframe
    :return: single of dataframes
    """
    # if data is empty return other records
    if len(updated) == 0:
        return created

    if len(created) == 0:
        return updated

    c = created.to_dict(orient="records")
    u = updated.to_dict(orient="records")
    tc = dict()
    for t in u:
        tmp = []
        for k in keys:
            tmp.append(t.get(k, None))

        # if all keys is none then skip
        if not all(tmp):
            continue

        tmp_key = ":".join(map(str, tmp))
        tc[tmp_key] = t

    # merge value to created
    for k, v in enumerate(c):
        tmp = []
        for kk in keys:
            tmp.append(v.get(kk, None))

        # if all keys is none then skip
        if not all(tmp):
            continue

        tmp_key = ":".join(map(str, tmp))
        if tc.get(tmp_key, None) is not None:
            c[k] = tc.pop(tmp_key, None)
            continue

    # concat value with update data
    c = c + [v for k, v in tc.items()]
    return pandas.DataFrame(c)


def _unique_object_from_list(
        data: List[Dict[str, Any]], keys: List[str]
) -> Dict[str, Any]:
    """Convert list of dictionary as unique dictionary
    :param data: list of dictionary
    :param keys: list of keys
    :return: dictionary data
    """
    from collections import defaultdict

    res = defaultdict()
    for rec in data:
        key = ":".join(map(lambda x: str(rec[x]), keys))
        res[key] = rec

    return dict(res)


def upsert_array(
        source: List[Dict[str, Any]], target: List[Dict[str, Any]], keys: List[str]
) -> List[Dict[str, Any]]:
    """Merge 2 array by keys and return as merged values
    :param source: list of dictionary
    :param target: list of dictionary
    :param keys: list of string
    :return: list of dictionary
    """
    unique_source = _unique_object_from_list(source, keys)
    unique_target = _unique_object_from_list(target, keys)
    unique_source.update(unique_target)
    return [dict(o) for o in unique_source.values()]


class DatetimeUtil:
    iso_format = "%Y-%m-%dT%H:%M:%SZ"

    @staticmethod
    def get_string_utc_time():
        return datetime.utcnow().strftime(DatetimeUtil.iso_format)

    @staticmethod
    def datetime_to_string(
            datetime_object: datetime,
    ) -> str:
        """Convert datetime object to string datetime with ISO format
        :param datetime_object: datetime object
        :return: string datetime with iso format
        """
        if isinstance(datetime_object, datetime):
            return datetime_object.strftime(DatetimeUtil.iso_format)


def validate_vdb_schema(
        data: pandas.DataFrame, url: str, model: Any
):
    cls = ConnectS3()
    tmp = []
    error_records = []
    for d in data.to_dict(orient="records"):
        try:
            parsed_data = model.parse_obj(d)
            tmp.append(parsed_data)
        except Exception as e:
            logging.error(
                "Error in model:{}, row:{}, Error:{}".format(model.__name__, str(d), e))
            error_records.append(d)
    if len(error_records) > 0:
        # save error records in S3
        e_records = pandas.DataFrame(error_records)
        now = datetime.now()
        filename = url.split("/")[-1]
        filepath = "vdb/error_records/{year}{month:02d}/{filename}".format(
            year=now.year,
            month=now.month,
            filename=filename
        )
        s3 = boto3.resource('s3')
        bucket = s3.Bucket(AwsConfig().s3_bucket_name)
        objs = list(bucket.objects.filter(Prefix=filepath))
        if len(objs) > 0:
            print("key exists!!")
            records = cls.read_csv_from_S3(
                bucket_name=AwsConfig().s3_bucket_name,
                object_name=filepath,
                resource=s3
            )
            records = pandas.concat([records, e_records])
            records = records.drop_duplicates(subset=records.columns, keep="first")
            cls.write_csv_to_S3(
                bucket_name=AwsConfig().s3_bucket_name,
                object_name=filepath,
                df_to_upload=records,
                resource=s3
            )
        else:
            print("key doesn't exist!")
            cls.write_csv_to_S3(
                bucket_name=AwsConfig().s3_bucket_name,
                object_name=filepath,
                df_to_upload=e_records,
                resource=s3
            )
    # convert and return validated data
    resp = pandas.DataFrame([o.__dict__ for o in tmp])
    return resp
