# vdb_producer_service

## Build and running container

```shell
docker build --rm \
    -f "Dockerfile" \
    -t vplus/vdb-producer:latest . && \
docker run --restart always \
     --network db-network \
     --name vdb-producer \
     -e ENV=local \
     -e AMQP_BROKER_URI=pyamqp://guest@local-rabbit// \
     -e TEST_DUMMY=1 \
     -e MYSQL_URI=mysql+pymysql://root:rahasia@local-mysql:3306/inventory \
     -e REDIS_URI=redis://:rahasia@local-redis:6379/0 \
     -d vplus/vdb-producer:latest
```

## Activate virtualenv
```shell
  poetry env use $(which python)
```

## Install dependencies
```shell
  poetry install
```

## Update dependencies
```shell
  poetry update
```

## Register private url MNC
```shell
  poetry config repositories.mnc ${privateUrl}
```

## setting username and password for publishing
```shell
    poetry config http-basic.mnc ${username} ${password}
```