import time

from app import app, cfg
from controllers.poster_banner import PosterBanner
from utils import custom_exception
from controllers import (
    HelloWorldController,
    PayTvProviderController,
    CustomerController,
    UserPayTVController,
    UserDetailHavingPackageController,
    ContentCoreSynopsisController,
    UserDetailHavingProductController,
    ProductHavingPackageController,
    SeasonController,
    ContentController,
    CountryController,
    ContentHavingCountryController,
    ProductController,
    CategoryController,
    SubCategoryController,
    ContentHavingActorController,
    ContentHavingDirectorController,
    ContentCoreController,
    ContentHavingTagController,
    TagsController,
    ActorController,
    ContentBundleController,
    ContentBundleHavingContentController,
    PackageController,
    PackageHavingContentBundleController,
    HomePageController,
    HomePageHavingContentController, RemoveFileController,

)
from repository import RedisConnection
from utils import Logging


def init_before_startup() -> bool:
    """prepare initialize before starting up
    :return: boolean true
    """
    Logging.info("initialize redis connection")
    RedisConnection.from_uri(cfg.redis_uri)
    return True


@app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    # initialize config before starting up
    while init_before_startup() is not True:
        time.sleep(1)


@app.task(
    ignore_result=True,
    name="{}.hello_world".format(cfg.amqp_routing_key),
    queue=cfg.queue_name,
)
@custom_exception()
def hello_world():
    Logging.info("""[hello_world] - received task""")
    cls = HelloWorldController()
    cls.send_message()


@app.task(
    ignore_result=True,
    name="{}.customer".format(cfg.amqp_routing_key),
    queue=cfg.queue_name,
)
@custom_exception()
def customer():
    Logging.info("""[customer] - received task""")
    cls = CustomerController()
    cls.fetch_data()


@app.task(
    ignore_result=True,
    name="{}.user_pay_tv".format(cfg.amqp_routing_key),
    queue=cfg.queue_name,
)
@custom_exception()
def user_pay_tv():
    Logging.info("""[user_pay_tv] - received task""")
    cls = UserPayTVController()
    cls.fetch_data()


@app.task(
    ignore_result=True,
    name="{}.user_detail_having_package".format(cfg.amqp_routing_key),
    queue=cfg.queue_name,
)
@custom_exception()
def user_detail_having_package():
    Logging.info("""[user_detail_having_package] - received task""")
    cls = UserDetailHavingPackageController()
    cls.fetch_data()


@app.task(
    ignore_result=True,
    name="{}.product".format(cfg.amqp_routing_key),
    queue=cfg.queue_name,
)
@custom_exception()
def product():
    Logging.info("""[product] - received task""")
    cls = ProductController()
    cls.fetch_data()


@app.task(
    ignore_result=True,
    name="{}.user_detail_having_product".format(cfg.amqp_routing_key),
    queue=cfg.queue_name,
)
@custom_exception()
def user_detail_having_product():
    Logging.info("""[user_detail_having_product] - received task""")
    cls = UserDetailHavingProductController()
    cls.fetch_data()


@app.task(
    ignore_result=True,
    name="{}.content".format(cfg.amqp_routing_key),
    queue=cfg.queue_name,
)
@custom_exception()
def content():
    Logging.info("""[content] - received task""")
    cls = ContentController()
    cls.fetch_data()


@app.task(
    ignore_result=True,
    name="{}.country".format(cfg.amqp_routing_key),
    queue=cfg.queue_name,
)
@custom_exception()
def country():
    Logging.info("""[country] - received task""")
    cls = CountryController()
    cls.fetch_data()


@app.task(
    ignore_result=True,
    name="{}.content_having_country".format(cfg.amqp_routing_key),
    queue=cfg.queue_name,
)
@custom_exception()
def content_having_country():
    Logging.info("""[content_having_country] - received task""")
    cls = ContentHavingCountryController()
    cls.fetch_data()


@app.task(
    ignore_result=True,
    name="{}.category".format(cfg.amqp_routing_key),
    queue=cfg.queue_name,
)
@custom_exception()
def category():
    Logging.info("""[category] - received task""")
    cls = CategoryController()
    cls.fetch_data()


@app.task(
    ignore_result=True,
    name="{}.subcategory".format(cfg.amqp_routing_key),
    queue=cfg.queue_name,
)
@custom_exception()
def subcategory():
    Logging.info("""[subcategory] - received task""")
    cls = SubCategoryController()
    cls.fetch_data()


@app.task(
    ignore_result=True,
    name="{}.content_having_actor".format(cfg.amqp_routing_key),
    queue=cfg.queue_name,
)
@custom_exception()
def content_having_actor():
    Logging.info("""[content_having_actor] - received task""")
    cls = ContentHavingActorController()
    cls.fetch_data()


@app.task(
    ignore_result=True,
    name="{}.content_having_director".format(cfg.amqp_routing_key),
    queue=cfg.queue_name,
)
@custom_exception()
def content_having_director():
    Logging.info("""[content_having_director] - received task""")
    cls = ContentHavingDirectorController()
    cls.fetch_data()


@app.task(
    ignore_result=True,
    name="{}.content_core".format(cfg.amqp_routing_key),
    queue=cfg.queue_name,
)
@custom_exception()
def content_core():
    Logging.info("""[content_core] - received task""")
    cls = ContentCoreController()
    cls.fetch_data()


@app.task(
    ignore_result=True,
    name="{}.content_having_tags".format(cfg.amqp_routing_key),
    queue=cfg.queue_name,
)
@custom_exception()
def content_having_tags():
    Logging.info("""[content_having_tags] - received task""")
    cls = ContentHavingTagController()
    cls.fetch_data()


@app.task(
    ignore_result=True,
    name="{}.tags".format(cfg.amqp_routing_key),
    queue=cfg.queue_name,
)
@custom_exception()
def tags():
    Logging.info("""[tags] - received task""")
    cls = TagsController()
    cls.fetch_data()


@app.task(
    ignore_result=True,
    name="{}.actor".format(cfg.amqp_routing_key),
    queue=cfg.queue_name,
)
@custom_exception()
def actor():
    Logging.info("""[actor] - received task""")
    cls = ActorController()
    cls.fetch_data()


@app.task(
    ignore_result=True,
    name="{}.content_bundle".format(cfg.amqp_routing_key),
    queue=cfg.queue_name,
)
@custom_exception()
def content_bundle():
    Logging.info("""[content_bundle] - received task""")
    cls = ContentBundleController()
    cls.fetch_data()


@app.task(
    ignore_result=True,
    name="{}.content_bundle_having_content".format(cfg.amqp_routing_key),
    queue=cfg.queue_name,
)
@custom_exception()
def content_bundle_having_content():
    Logging.info("""[content_bundle_having_content] - received task""")
    cls = ContentBundleHavingContentController()
    cls.fetch_data()


@app.task(
    ignore_result=True,
    name="{}.package".format(cfg.amqp_routing_key),
    queue=cfg.queue_name,
)
@custom_exception()
def package():
    Logging.info("""[package] - received task""")
    cls = PackageController()
    cls.fetch_data()


@app.task(
    ignore_result=True,
    name="{}.package_having_content_bundle".format(cfg.amqp_routing_key),
    queue=cfg.queue_name,
)
@custom_exception()
def package_having_content_bundle():
    Logging.info("""[package_having_content_bundle] - received task""")
    cls = PackageHavingContentBundleController()
    cls.fetch_data()


@app.task(
    ignore_result=True,
    name="{}.content_core_synopsis".format(cfg.amqp_routing_key),
    queue=cfg.queue_name,
)
@custom_exception()
def content_core_synopsis():
    Logging.info("""[content_core_synopsis] - received task""")
    cls = ContentCoreSynopsisController()
    cls.fetch_data()


@app.task(
    ignore_result=True,
    name="{}.product_having_package".format(cfg.amqp_routing_key),
    queue=cfg.queue_name,
)
@custom_exception()
def product_having_package():
    Logging.info("""[product_having_package] - received task""")
    cls = ProductHavingPackageController()
    cls.fetch_data()


@app.task(
    ignore_result=True,
    name="{}.season".format(cfg.amqp_routing_key),
    queue=cfg.queue_name,
)
@custom_exception()
def season():
    Logging.info("""[season] - received task""")
    cls = SeasonController()
    cls.fetch_data()


@app.task(
    ignore_result=True,
    name="{}.homepage".format(cfg.amqp_routing_key),
    queue=cfg.queue_name,
)
@custom_exception()
def homepage():
    Logging.info("""[homepage] - received task""")
    cls = HomePageController()
    cls.fetch_data()


@app.task(
    ignore_result=True,
    name="{}.homepage_having_content".format(cfg.amqp_routing_key),
    queue=cfg.queue_name,
)
@custom_exception()
def homepage_having_content():
    Logging.info("""[homepage_having_content] - received task""")
    cls = HomePageHavingContentController()
    cls.fetch_data()


@app.task(
    ignore_result=True,
    name="{}.paytv_provider".format(cfg.amqp_routing_key),
    queue=cfg.queue_name,
)
@custom_exception()
def paytv_provider():
    Logging.info("""[paytv_provider] - received task""")
    cls = PayTvProviderController()
    cls.fetch_data()


@app.task(
    ignore_result=True,
    name="{}.remove_file".format(cfg.amqp_routing_key),
    queue=cfg.queue_name,
)
def remove_file():
    Logging.info("""[remove_file] - received task""")
    cls = RemoveFileController()
    cls.remove_file()

@app.task(
    ignore_result=True,
    name="{}.poster_banner_paytv".format(cfg.amqp_routing_key),
    queue=cfg.queue_name,
)
def poster_banner_paytv():
    Logging.info("""[poster_banner_paytv] - received task""")
    cls = PosterBanner()
    cls.pay_tv_content_poster_banner()


@app.task(
    ignore_result=True,
    name="{}.poster_banner_no_paytv".format(cfg.amqp_routing_key),
    queue=cfg.queue_name,
)
def poster_banner_no_paytv():
    Logging.info("""[poster_banner_no_paytv] - received task""")
    cls = PosterBanner()
    cls.no_pay_tv_content_poster_banner()

