#!/bin/bash
celery -A main worker \
            --concurrency=1 \
            --autoscale=1 \
            --max-tasks-per-child=100 \
            --queues=vdb_producer \
            --loglevel=info \
            -s ./celerybeat-schedule \
            -n worker@%h