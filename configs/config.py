import configparser
import json
import logging
import os
from logging.handlers import RotatingFileHandler
from typing import Any, List
from typing import Dict

from cachetools import cached, LRUCache
from pydantic import BaseSettings

config = configparser.RawConfigParser()
config.read("{}/{}".format(os.path.join(os.path.dirname(__file__)), "config.ini"))
ENV = os.environ.get("ENV", "uat")


class AwsConfig(BaseSettings):
    s3_bucket_name: str = os.getenv(
        "AWS_BUCKET_NAME", config.get(ENV, "s3_bucket_name")
    )
    s3_object_name: str = os.getenv("S3_OBJECT_NAME", config.get(ENV, "s3_object_name"))
    aws_access_key_id: str = os.getenv(
        "AWS_ACCESS_KEY_ID", config.get(ENV, "aws_access_key_id")
    )
    aws_secret_access_key: str = os.getenv(
        "AWS_SECRET_ACCESS_KEY", config.get(ENV, "aws_secret_access_key")
    )
    region_name: str = os.getenv("AWS_REGION_NAME", config.get(ENV, "region_name"))
    having_package_mapping: str = os.getenv(
        "HAVING_PACKAGE_MAPPING", config.get(ENV, "having_package_mapping")
    )
    vdb_module_path: str = os.getenv(
        "VDB_MODULE_PATH", config.get(ENV, "vdb_module_path")
    )


class Config(BaseSettings):
    env: str = os.getenv("ENV", config.get(ENV, "env", fallback="dev"))
    logging_type: str = os.getenv(
        "LOGGING_TYPE", config.get(ENV, "logging_type", fallback="INFO")
    )
    worker_name: str = os.getenv(
        "WORKER_NAME", config.get(ENV, "worker_name", fallback="")
    )
    queue_name: str = os.getenv(
        "QUEUE_NAME", config.get(ENV, "queue_name", fallback="")
    )
    consumer_queue_name: str = os.getenv(
        "CONSUMER_QUEUE_NAME", config.get(ENV, "consumer_queue_name", fallback="")
    )
    consumer_routing_name: str = os.getenv(
        "CONSUMER_ROUTING_NAME",
        config.get(ENV, "consumer_routing_name", fallback=""),
    )
    amqp_broker_uri: str = os.getenv(
        "AMQP_BROKER_URI", config.get(ENV, "amqp_broker_uri", fallback="")
    )
    amqp_backend_uri: str = os.getenv(
        "AMQP_BACKEND_URI", config.get(ENV, "amqp_backend_uri", fallback="")
    )
    amqp_routing_key: str = os.getenv(
        "AMQP_ROUTING_KEY", config.get(ENV, "amqp_routing_key", fallback="")
    )
    graph_conn_uri_writer: str = os.getenv(
        "GRAPH_CONN_URI_WRITER", config.get(ENV, "graph_conn_uri_writer", fallback="")
    )
    graph_conn_uri_reader: str = os.getenv(
        "GRAPH_CONN_URI_READER", config.get(ENV, "graph_conn_uri_reader", fallback="")
    )
    redis_uri: str = os.getenv("REDIS_URI", config.get(ENV, "redis_uri", fallback=""))
    redis_uri_recommendation: str = os.getenv(
        "REDIS_URI_RECOMMENDATION", config.get(ENV, "redis_uri_recommendation", fallback="")
    )
    s3_bucket_name: str = str(
        os.getenv(
            "AWS_BUCKET_NAME",
            config.get(ENV, "s3_bucket_name", fallback=""),
        ),
    )
    aws_access_key_id: str = str(
        os.getenv(
            "AWS_ACCESS_KEY_ID",
            config.get(ENV, "aws_access_key_id", fallback=""),
        ),
    )
    aws_secret_access_key: str = str(
        os.getenv(
            "AWS_SECRET_ACCESS_KEY",
            config.get(ENV, "aws_secret_access_key", fallback=""),
        ),
    )
    region_name: str = str(
        os.getenv(
            "AWS_REGION_NAME",
            config.get(ENV, "region_name", fallback=""),
        ),
    )
    redis_cache_lifetime: int = int(
        os.getenv(
            "REDIS_CACHE_LIFETIME",
            config.get(ENV, "redis_cache_lifetime", fallback=60 * 5),
        ),
    )
    mnc_cloud_username: str = os.getenv(
        "MNC_CLOUD_USERNAME", config.get(ENV, "mnc_cloud_username", fallback="")
    )
    mnc_cloud_password: str = os.getenv(
        "MNC_CLOUD_PASSWORD", config.get(ENV, "mnc_cloud_password", fallback="")
    )
    mnc_path_file: str = os.getenv(
        "MNC_PATH_FILE", config.get(ENV, "mnc_path_file", fallback="")
    )
    http_base_url: str = os.getenv(
        "HTTP_BASE_URL", config.get(ENV, "http_base_url", fallback="")
    )
    http_client_timeout: int = os.getenv(
        "HTTP_CLIENT_TIMEOUT",
        config.getint(ENV, "http_client_timeout", fallback=10),
    )
    http_chunk_data: int = int(
        os.getenv(
            "HTTP_CHUNK_DATA", config.getint(ENV, "http_chunk_data", fallback=1_000)
        )
    )
    content_detail_api: str = str(
        os.getenv(
            "CONTENT_DETAIL_API",
            config.get(ENV, "content_detail_api", fallback=""),
        ),
    )
    content_detail_api_token: str = str(
        os.getenv(
            "CONTENT_DETAIL_API_TOKEN",
            config.get(ENV, "content_detail_api_token", fallback=""),
        ),
    )
    os.environ["ENV"] = ENV
    os.environ["REDIS_URI"] = redis_uri
    os.environ["REDIS_URI_RECOMMENDATION"] = redis_uri_recommendation
    os.environ["GRAPH_CONN_URI_WRITER"] = graph_conn_uri_writer
    os.environ["GRAPH_CONN_URI_READER"] = graph_conn_uri_reader
    os.environ["AWS_ACCESS_KEY_ID"] = aws_access_key_id
    os.environ["AWS_REGION_NAME"] = region_name
    os.environ["AWS_SECRET_ACCESS_KEY"] = aws_secret_access_key
    os.environ["AWS_BUCKET_NAME"] = s3_bucket_name

    @staticmethod
    @cached(cache=LRUCache(maxsize=128))
    def get_config() -> Dict[str, Any]:
        """Get default configuration
        for sensitive module name in config we will use some hashing with base64 method
        :return: dictionary data configuration
        """
        return Config().dict()

    @staticmethod
    @cached(cache=LRUCache(maxsize=128))
    def logging_handler() -> List[Any]:
        return [
            RotatingFileHandler(
                filename="logs/app.log",
                mode="w",
                maxBytes=512000,
                backupCount=4,
            ),
            logging.StreamHandler(),
        ]

    @staticmethod
    @cached(cache=LRUCache(maxsize=128))
    def get_logger_format() -> str:
        return json.dumps(
            {
                "datetime": "%(asctime)s",
                "level_name": "%(levelname)s",
                "pathname": "%(pathname)s",
                "func_name": "%(funcName)s",
                "module": "%(module)s",
                "lineno": "%(lineno)d",
                "message": "%(message)s",
            }
        )

    @staticmethod
    @cached(cache=LRUCache(maxsize=128))
    def get_logging_type() -> Dict[str, Any]:
        """Get logging type
        :return: dictionary data
        """
        return {
            "DEBUG": logging.DEBUG,
            "INFO": logging.INFO,
            "ERROR": logging.ERROR,
            "FATAL": logging.FATAL,
        }

