import redis

# Duplicate Code: Connection Configuration
r = redis.StrictRedis(host='localhost', port=6379, db=0)

# Duplicate Code: Fetch Data for Users
user_data = r.get('user:123')
print(user_data)

# Duplicate Code: Fetch Data for Products
product_data = r.get('product:456')
print(product_data)

# Duplicate Code: Fetch Data for Orders
order_data = r.get('order:789')
print(order_data)

# Duplicate Code: Fetch Data for Invoices
invoice_data = r.get('invoice:101')
print(invoice_data)

# Duplicate Code: Close the Redis Connection
r.connection_pool.disconnect()
