from celery import Celery
from kombu import Queue, Exchange

from configs import Config

# load config
cfg = Config()

# setup celery for producer
app = Celery(
    cfg.worker_name,
    broker=cfg.amqp_broker_uri,
    backend=cfg.amqp_backend_uri,
)

# setup serializer
app.conf.update(
    task_serializer="pickle",
    accept_content=["pickle", "json"],
    result_serializer="pickle",
    # Increasing task time limit to 24 hours
    task_soft_time_limit=60 * 60 * 24,
    task_time_limit=60 * 60 * 24,
    task_acks_on_failure_or_timeout=False,
)

# setup celery task routes for producer
app.task_routes = (
    Queue(
        cfg.queue_name,
        Exchange("transient", type="direct", delivery_mode=1),
        routing_key="{}.*".format(cfg.amqp_routing_key),
    ),
)
