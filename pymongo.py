import pymongo

# Duplicate Code: Connection Configuration
client = pymongo.MongoClient("mongodb://localhost:27017")
db = client["mydb"]
collection = db["mycollection"]

# Duplicate Code: Fetch Data for Users
users = collection.find({"role": "user"})
for user in users:
    print(user)

# Duplicate Code: Fetch Data for Products
products = collection.find({"category": "electronics"})
for product in products:
    print(product)

# Duplicate Code: Fetch Data for Orders
orders = collection.find({"status": "pending"})
for order in orders:
    print(order)

# Duplicate Code: Fetch Data for Invoices
invoices = collection.find({"paid": False})
for invoice in invoices:
    print(invoice)

# Duplicate Code: Close the MongoDB Connection
client.close()
