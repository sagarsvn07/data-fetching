import gzip
import os
import pickle
from io import BytesIO
from typing import Union, BinaryIO, Dict, Any
from pandas import DataFrame
import boto3
from configs import AwsConfig
from utils import Logging


class S3Services:
    bucket_name: str
    client: boto3.client
    resource: boto3.resource

    def __init__(self):
        S3Services.client = boto3.client(
            "s3",
            region_name=AwsConfig().region_name,
            aws_access_key_id=AwsConfig().aws_access_key_id,
            aws_secret_access_key=AwsConfig().aws_secret_access_key,

        )
        S3Services.bucket_name = AwsConfig().s3_bucket_name

        S3Services.resource = boto3.resource(
            "s3",
            region_name=AwsConfig().region_name,
            aws_access_key_id=AwsConfig().aws_access_key_id,
            aws_secret_access_key=AwsConfig().aws_secret_access_key,
        )

    @staticmethod
    def read_files(bucket_name: str, file_name: str) -> bytes:
        """Read value from s3 and return value as string
        :param bucket_name: string bucket name
        :param file_name: file name is s3
        :return: string value
        """
        file_bytes = S3Services.client.get_object(Bucket=bucket_name, Key=file_name)
        if v := file_bytes.get("Body", None):
            return v.read()

        return bytes()

    @staticmethod
    def write_files(
        files: Union[bytes, BinaryIO, str],
        bucket_name: str,
        file_name: str,
    ) -> Dict[str, Any]:
        """Update files blob to s3
        :param files: bytes data types
        :param bucket_name: string bucket name
        :param file_name: string file name in s3
        :return: dictionary response
        """
        return S3Services.client.put_object(
            Body=files,
            Bucket=bucket_name,
            Key=file_name,
        )

    @staticmethod
    def read_compress_pickles_from_S3(
            object_name=None,
    ) -> DataFrame:
        """Read pickle as compressed file
        :param bucket_name: bucket name
        :param object_name: S3 Path of Pickle file
        :param resource: Connection object
        :return:dataframe
        """
        try:
            content_object = S3Services.resource.Object(S3Services.bucket_name, object_name)
            read_file = content_object.get()["Body"].read()
            zipfile = BytesIO(read_file)
            with gzip.GzipFile(fileobj=zipfile) as gzipfile:
                content = gzipfile.read()

            loaded_pickle = pickle.loads(content)
            print("File {} has been read successfully".format(object_name))
            return loaded_pickle
        except Exception as e:
            Logging.error(f"Error while reading {object_name} to S3, Exception: {e}")

    @staticmethod
    def write_df_to_pkl_S3(
             object_name=None, data=None
    ) -> None:
        """Upload csv  as compressed  pickle file
        :param bucket_name: bucket name
        :param object_name:Where to upload
        :param data : dataframe
        :param resource: Connection object
        :return:None
        """
        try:
            file_name = os.path.split(object_name)[1]
            Logging.info("Start dumping " + file_name + " data into s3")
            pickle_buffer = BytesIO()
            data.to_pickle(pickle_buffer, compression="gzip")
            S3Services.resource.Object(S3Services.bucket_name, object_name).put(Body=pickle_buffer.getvalue())
            Logging.info("Successfully dumped " + file_name + " data into s3")
        except Exception as e:
            Logging.error(f"Error while dumping {object_name} to S3, Exception: {e}")
