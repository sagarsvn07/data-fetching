from typing import ClassVar, List

import pandas as pd

from app import cfg
from configs.config import ENV
from services.http_services import HttpServices
from utils import Logging, decode_string


class MNCCloudServices:
    def __init__(
        self,
        base_url: str,
    ):
        self.client = HttpServices.from_base_url(base_url)

    @classmethod
    def from_base_url(cls, base_url: str) -> ClassVar:
        """Create new object based on specified base url
        :param base_url: string base url
        :return: current object class
        """
        return cls(base_url)

    def delete_and_move_data_after_read(
        self,
        url: str,
        tmp_url: str,
        data: bytes,
        username: str = None,
        password: str = None,
    ) -> bool:
        """After read data then delete file from real path and move to temporary folder
        :param url: string url
        :param tmp_url: string archive data that will used as archiving data
        :param data: bytes data
        :param username: string username
        :param password: string password
        :return:
        """
        # first upload to new_directory
        url = self.parse_url(url)
        tmp_url = self.parse_url(tmp_url)
        self.upload_file(url=tmp_url, data=data, username=username, password=password)
        # then delete original file
        self.delete_file_after_read(url, username, password)
        return True

    def upload_file(
        self,
        url: str,
        data: bytes,
        username: str = None,
        password: str = None,
    ) -> bool:
        """Delete file from cloud after reading and publishing data
        :param url: string url to delete
        :param data: bytes data that will update
        :param username: string username to access this data in cloud
        :param password: string password to access this data in cloud
        :return: boolean true or false
        """
        url = self.parse_url(url)
        return self.client.put(url=url, data=data, username=username, password=password)

    def delete_file_after_read(
        self, url: str, username: str = None, password: str = None
    ) -> bool:
        """Delete file from cloud after reading and publishing data
        :param url: string url to delete
        :param username: string username to access this data in cloud
        :param password: string password to access this data in cloud
        :return: boolean true or false
        """
        url = self.parse_url(url)

        return self.client.delete(url, username, password)

    def parse_url(
            self, url: str,
    )-> str:
        if ENV == 'uat':
            if decode_string(cfg.mnc_cloud_username) in url:
                return '/'.join(url.split('/')[1:])
        return url


    def fetch_csv_data(
        self, url: str, username: str = None, password: str = None
    ) -> pd.DataFrame:
        """Fetch data from http services with specified url
        :param url: string url
        :param username: string username
        :param password: string password
        :return: list of dictionary
        """
        url = self.parse_url(url)
        csv = self.client.load_to_dataframe(url, username, password)
        Logging.info("csv {} is loaded completely".format(url).lower())
        return csv

    def fetch_list(
            self,
            url: str = None,
            username: str = None,
            password: str = None,
    ) -> List[str]:
        """Delete file from cloud after reading and publishing data
        :param url: string url to delete
        :param username: string username to access this data in cloud
        :param password: string password to access this data in cloud
        :param required_list:string list to access only required table
        :return: list
        """
        url = self.parse_url(url)
        return self.client.list_data(url=url, username=username, password=password)

