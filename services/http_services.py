import io
from http import HTTPStatus
from typing import List, Dict, Any
import xmltodict
import pandas as pd
import requests
from requests.auth import HTTPBasicAuth
from requests.exceptions import RequestException

from app import cfg
from utils import Logging


class HttpServices:
    def __init__(self, base_url: str):
        # if base url have end with slash and then remove this slash
        # example http://localhost/
        # then it will remove las slash to like this http://localhost
        if base_url.endswith("/"):
            base_url = base_url[:-1]

        self.base_url = base_url
        self.cl = requests.session()
        self.default_time_out = cfg.http_client_timeout
        self.default_chunk_value = cfg.http_chunk_data

    @classmethod
    def from_base_url(cls, base_url: str) -> "HttpServices":
        """Create new object based on specified base url
        :param base_url: string base url
        :return: current object class
        """
        return cls(base_url)

    def put(
        self,
        url: str,
        username: str = None,
        password: str = None,
        data: bytes = None,
    ) -> List[Dict[str, Any]]:
        """Fetch data from http services and return as list of dictionary
        :param url: string url
        :param username: string url
        :param password: string url
        :param data: bytes data to update
        :return: list of dictionary
        """
        try:
            # if base url have end with slash and then remove this slash
            # example /user/123
            # then it will remove las slash to like this user/123
            if url.startswith("/"):
                url = url[1:]

            url = "{}/{}".format(self.base_url, url)
            if username is not None and password is not None:
                resp = self.cl.put(
                    url,
                    timeout=self.default_time_out,
                    data=data,
                    auth=HTTPBasicAuth(username, password),
                )
                if resp.status_code == HTTPStatus.NOT_FOUND:
                    return []

                return resp.json()

            resp = self.cl.put(url, timeout=self.default_time_out, data=data)
            if resp.status_code == HTTPStatus.NOT_FOUND:
                return []

            return resp.json()
        except RequestException as t:
            return []

    def get(
        self,
        url: str,
        username: str = None,
        password: str = None,
    ) -> List[Dict[str, Any]]:
        """Fetch data from http services and return as list of dictionary
        :param url: string url
        :param username: string url
        :param password: string url
        :return: list of dictionary
        """
        try:
            # if base url have end with slash and then remove this slash
            # example /user/123
            # then it will remove las slash to like this user/123
            if url.startswith("/"):
                url = url[1:]

            url = "{}/{}".format(self.base_url, url)
            if username is not None and password is not None:
                resp = self.cl.get(
                    url,
                    timeout=self.default_time_out,
                    auth=HTTPBasicAuth(username, password),
                )
                if resp.status_code == HTTPStatus.NOT_FOUND:
                    return []

                return resp.json()

            resp = self.cl.get(url, timeout=self.default_time_out)
            if resp.status_code == HTTPStatus.NOT_FOUND:
                return []

            return resp.json()
        except RequestException as t:
            return []

    def delete(
        self,
        url: str,
        username: str = None,
        password: str = None,
    ) -> bool:
        """Fetch data from http services and return as list of dictionary
        :param url: string url
        :param username: string url
        :param password: string url
        :return: boolean true or false
        """
        try:
            # if base url have end with slash and then remove this slash
            # example /user/123
            # then it will remove las slash to like this user/123
            if url.startswith("/"):
                url = url[1:]

            url = "{}/{}".format(self.base_url, url)
            if username is not None and password is not None:
                resp = self.cl.delete(
                    url,
                    timeout=self.default_time_out,
                    auth=HTTPBasicAuth(username, password),
                )
                return resp.status_code == 200

            resp = self.cl.delete(url, timeout=self.default_time_out)
            return resp.status_code == 200
        except RequestException as t:
            return False

    def get_raw(
        self,
        url: str,
        username: str = None,
        password: str = None,
    ) -> bytes:
        """Fetch data from http services and return as list of dictionary
        :param url: string url
        :param username: string url
        :param password: string url
        :return: bytes data
        """
        try:
            # if base url have end with slash and then remove this slash
            # example /user/123
            # then it will remove las slash to like this user/123
            if url.startswith("/"):
                url = url[1:]

            url = "{}/{}".format(self.base_url, url)
            if username is not None and password is not None:
                resp = self.cl.get(
                    url,
                    timeout=self.default_time_out,
                    auth=HTTPBasicAuth(username, password),
                )
                if resp.status_code == HTTPStatus.NOT_FOUND:
                    return b""

                return resp.content

            resp = self.cl.get(url, timeout=self.default_time_out)
            if resp.status_code == HTTPStatus.NOT_FOUND:
                return b""

            return resp.content
        except RequestException as t:
            return b""

    def load_to_dataframe(
        self,
        url: str,
        username: str = None,
        password: str = None,
    ) -> pd.DataFrame:
        """Load csv from url and convert as dataframe
        we will chunk load data cause the data can be huge
        example:
            cls = HttpServices.from_base_url("https://cloud.mnc-cloud.xyz")
            resp = cls.load_to_dataframe("index.php/s/3i4TnZS38xRNHfB/download")
            for df in resp:
                print(len(df))
        :param url: string url
        :param username: string username
        :param password: string password
        :return: dataframe object generator
        """
        # if base url have end with slash and then remove this slash
        # example /user/123
        # then it will remove las slash to like this user/123
        if url.startswith("/"):
            url = url[1:]

        # get raw data from http
        if username is not None and password is not None:
            data = self.get_raw(url, username, password)
            # ida data is empty
            if len(data) == 0:
                return pd.DataFrame()

            resp = pd.read_csv(
                io.BytesIO(data),
                on_bad_lines="skip",
            )
            return resp

        data = self.get_raw(url)
        # ida data is empty
        if len(data) == 0:
            return pd.DataFrame()

        resp = pd.read_csv(
            io.BytesIO(data),
            on_bad_lines="skip",
        )
        return resp


    def list_data(self, url: str,
                  username: str = None,
                  password: str = None,
                  required_list:str = None,
                  endswith:str = None) -> List[str]:
        try:
            # Remove leading slash from URL
            url = url.lstrip("/")
            url_2 = f"{self.base_url}/{url}/"

            resp = requests.request(
                "PROPFIND",
                url_2,
                timeout=10,
                auth=HTTPBasicAuth(username, password),
                headers={
                    "Depth": "1",
                    "Content-Type": "application/json",
                    "Accept": "application/json",
                },
            )

            xml_content = resp.content.decode("utf-8")
            xml_dict = xmltodict.parse(xml_content)

            csv_list = [
                i["d:href"].split("/")[-1]
                for i in xml_dict["d:multistatus"]["d:response"]
                if "csv" in i["d:href"]
            ]

            return csv_list

        except TypeError:
            return []
        except Exception as e:
            Logging.error(str(e))
            return []
