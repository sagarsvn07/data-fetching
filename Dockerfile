FROM python:3.8.11-slim-buster as compile-image

## virtualenv
ENV VIRTUAL_ENV=/opt/venv
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

# install gcc
RUN apt update -y && \
    apt install -y build-essential && \
    apt install -y curl

RUN curl -sSL https://install.python-poetry.org | python3 -
ENV PATH="/root/.local/bin:$PATH"

WORKDIR /tmp/app
# copy and install requirements
COPY poetry.lock pyproject.toml ./
RUN poetry export --without-hashes --format=requirements.txt > requirements.txt && \
    pip install --upgrade pip && \
    pip install --no-cache-dir -r requirements.txt


FROM python:3.8.11-slim-buster as runtime-image

# set default timezone as utc
ENV TZ=UTC
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# internal env variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV PATH="/opt/venv/bin:$PATH"
ENV C_FORCE_ROOT 1

## set working directory
WORKDIR /tmp/app
## copy Python dependencies from build image
COPY --from=compile-image /opt/venv /opt/venv
## add app
COPY . /tmp/app
# doing obfuscate in code
RUN pyarmor obfuscate --recursive main.py && \
    cp -r ./dist /usr/src/app && \
    cp ./configs/config.ini /usr/src/app/configs/config.ini && \
    rm -rf /tmp/app

# change working directory
WORKDIR /usr/src/app
# create log folder
RUN mkdir /usr/src/app/logs

## Entry Point
ADD start-script.sh /start-script.sh
RUN chmod 755 /start-script.sh

CMD ["/start-script.sh"]