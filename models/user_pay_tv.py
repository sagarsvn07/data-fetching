import io
import uuid
from datetime import datetime, timedelta
from typing import List, Dict, Any, Tuple

import pandas as pd
from pydantic import Field
from vdb_schema import UserPayTVModel as BaseUserPayTVModel

from app import cfg
from repository import RedisConnection
from services import MNCCloudServices
from utils import decode_string, generate_ud_key
from utils.extract_csv import MNCCloudVDBService
from utils.util import validate_vdb_schema


def generate_sequence() -> str:
    """Generate sequence number based on specified module
    :return: string sequence number
    """
    return RedisConnection.generate_sequence_number("VDB_USER_PAY_TV_MODEL")


class UserPayTVModel(BaseUserPayTVModel):
    message_id: str = Field(default=uuid.uuid4().hex)
    sequence_number: str = Field(default_factory=generate_sequence)
    message_created_on: datetime = Field(default=datetime.utcnow().timestamp())


class UserPayTVRepository:
    def __init__(self):
        self.file_name = "user_pay_tv"
        self.today_date = datetime.now()
        self.yesterday_date = self.today_date - timedelta(days=1)
        self.upload_date_format = "{}{month:02d}{day:02d}".format(
            self.yesterday_date.year,
            # this format makes month 2 digits (eg: 5 -> 05, 6 -> 06)
            month=self.yesterday_date.month,
            day=self.yesterday_date.day,
        )
        self.username = decode_string(cfg.mnc_cloud_username)
        self.password = decode_string(cfg.mnc_cloud_password)
        self.services = MNCCloudServices.from_base_url(cfg.http_base_url)
        self.mnc_cloud_path_file = cfg.mnc_path_file
        self.features = MNCCloudVDBService()

    @property
    def get_url_created(self) -> str:
        """Get url created data
        :return: string url
        """
        url_created = "{}/{}/{}_{}_created.csv".format(
            self.username,
            self.mnc_cloud_path_file,
            self.file_name,
            self.upload_date_format,
        )
        return url_created

    @property
    def get_url_updated(self) -> str:
        """Get url updated data
        :return: string url
        """
        url_updated = "{}/{}/{}_{}_updated.csv".format(
            self.username,
            self.mnc_cloud_path_file,
            self.file_name,
            self.upload_date_format,
        )
        return url_updated

    @property
    def get_url_archived_created(self) -> str:
        """Get url created data
        :return: string url
        """
        url_created = "{}/{}/tmp/{}_{}_created.csv".format(
            self.username,
            self.mnc_cloud_path_file,
            self.file_name,
            self.upload_date_format,
        )
        return url_created

    @property
    def get_url_archived_updated(self) -> str:
        """Get url updated data
        :return: string url
        """
        url_updated = "{}/{}/tmp/{}_{}_updated.csv".format(
            self.username,
            self.mnc_cloud_path_file,
            self.file_name,
            self.upload_date_format,
        )
        return url_updated

    def get_user_pay_tv(
        self,
    ) -> Tuple[pd.DataFrame, pd.DataFrame]:
        """Get data from database and return as list of dictionary data
        :return: list of dictionary
        """
        url_created = "{}/{}/{}_{}_created.csv".format(
            self.username,
            self.mnc_cloud_path_file,
            self.file_name,
            self.upload_date_format,
        )
        created_file = self.services.fetch_csv_data(
            url_created, self.username, self.password
        )
        created_file = validate_vdb_schema(data=created_file, url=url_created, model=BaseUserPayTVModel)
        url_updated = "{}/{}/{}_{}_updated.csv".format(
            self.username,
            self.mnc_cloud_path_file,
            self.file_name,
            self.upload_date_format,
        )
        updated_file = self.services.fetch_csv_data(
            url_updated, self.username, self.password
        )
        updated_file = validate_vdb_schema(data=updated_file, url=url_updated, model=BaseUserPayTVModel)
        return created_file, updated_file

    def archiving_data_user_pay_tv(
        self,
        created_data: pd.DataFrame,
        updated_data: pd.DataFrame,
    ) -> bool:
        """Archiving data created and updated after reading
        :param created_data:
        :param updated_data:
        :return:
        """
        # created data bytes
        bytes_created = io.BytesIO()
        bytes_created.seek(0)
        created_data.to_csv(bytes_created)

        # updated data bytes
        bytes_updated = io.BytesIO()
        bytes_updated.seek(0)
        updated_data.to_csv(bytes_updated)

        # change path and add tmp
        self.services.delete_and_move_data_after_read(
            url=self.get_url_created,
            tmp_url=self.get_url_archived_created,
            data=bytes_created.getvalue(),
            username=self.username,
            password=self.password,
        )

        # change path and add tmp
        self.services.delete_and_move_data_after_read(
            url=self.get_url_updated,
            tmp_url=self.get_url_archived_updated,
            data=bytes_updated.getvalue(),
            username=self.username,
            password=self.password,
        )
        return True

    def get_user_pay_tv_list(self) -> Tuple[pd.DataFrame, pd.DataFrame]:
        created, updated = self.features.extract_all_csv_data(file_name=self.file_name, model=BaseUserPayTVModel)
        return created, updated
