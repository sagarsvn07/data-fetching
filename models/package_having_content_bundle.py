import uuid
from datetime import datetime, timedelta
from typing import List, Dict, Any, Tuple

import pandas
from pydantic import Field
from vdb_schema import (
    PackageHavingContentBundleModel as BasePackageHavingContentBundleModel,
)

from app import cfg
from repository import RedisConnection
from services import MNCCloudServices
from utils import DatetimeUtil, decode_string


def generate_sequence() -> str:
    """Generate sequence number based on specified module
    :return: string sequence number
    """
    return RedisConnection.generate_sequence_number(
        "VDB_PACKAGE_HAVING_CONTENT_BUNDLE_MODEL"
    )


class PackageHavingContentBundleModel(BasePackageHavingContentBundleModel):
    message_id: str = Field(default=uuid.uuid4().hex)
    sequence_number: str = Field(default_factory=generate_sequence)
    message_created_on: datetime = Field(default=datetime.utcnow().timestamp())


class PackageHavingContentBundleRepository:
    def __init__(self):
        self.file_name = "package_having_content_bundle"
        self.today_date = datetime.now()
        self.yesterday_date = self.today_date - timedelta(days=1)
        self.upload_date_format = "{}{month:02d}{day:02d}".format(
            self.yesterday_date.year,
            # this format makes month 2 digits (eg: 5 -> 05, 6 -> 06)
            month=self.yesterday_date.month,
            day=self.yesterday_date.day,
        )
        self.username = decode_string(cfg.mnc_cloud_username)
        self.password = decode_string(cfg.mnc_cloud_password)
        self.services = MNCCloudServices.from_base_url(cfg.http_base_url)
        self.mnc_cloud_path_file = cfg.mnc_path_file

    def get_package_having_content_bundle(
        self,
    ) -> Tuple[pandas.DataFrame, pandas.DataFrame]:
        """Get data from database and return as list of dictionary data
        :return: list of dictionary
        """
        url_created = "{}/{}/{}_{}_created.csv".format(
            self.username,
            self.mnc_cloud_path_file,
            self.file_name,
            self.upload_date_format,
        )
        created_file = self.services.fetch_csv_data(
            url_created, self.username, self.password
        )
        url_updated = "{}/{}/{}_{}_updated.csv".format(
            self.username,
            self.mnc_cloud_path_file,
            self.file_name,
            self.upload_date_format,
        )
        updated_file = self.services.fetch_csv_data(
            url_updated, self.username, self.password
        )
        return created_file, updated_file
