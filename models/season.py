import uuid
from datetime import datetime, timedelta
from typing import List, Dict, Any, Tuple

import pandas
import pandas as pd
from pydantic import Field
from vdb_schema import SeasonModel as BaseSeasonModel

from app import cfg
from repository import RedisConnection
from services import MNCCloudServices
from utils import decode_string


def generate_sequence() -> str:
    """Generate sequence number based on specified module
    :return: string sequence number
    """
    return RedisConnection.generate_sequence_number("VDB_SEASON_MODEL")


class SeasonModel(BaseSeasonModel):
    message_id: str = Field(default=uuid.uuid4().hex)
    sequence_number: str = Field(default_factory=generate_sequence)
    message_created_on: datetime = Field(default=datetime.utcnow().timestamp())


class SeasonRepository:
    def __init__(self):
        self.file_name = "season"
        self.today_date = datetime.now()
        self.yesterday_date = self.today_date - timedelta(days=1)
        self.upload_date_format = "{}{month:02d}{day:02d}".format(
            self.yesterday_date.year,
            # this format makes month 2 digits (eg: 5 -> 05, 6 -> 06)
            month=self.yesterday_date.month,
            day=self.yesterday_date.day,
        )
        self.username = decode_string(cfg.mnc_cloud_username)
        self.password = decode_string(cfg.mnc_cloud_password)
        self.services = MNCCloudServices.from_base_url(cfg.http_base_url)
        self.mnc_cloud_path_file = cfg.mnc_path_file

    def get_season(
        self,
    ) -> Tuple[pandas.DataFrame, pandas.DataFrame]:
        """Get data from database and return as list of dictionary data
        :return: list of dictionary
        """
        url_all = "{}/{}/{}_{}.csv".format(
            self.username,
            self.mnc_cloud_path_file,
            self.file_name,
            self.upload_date_format,
        )
        _ = pd.DataFrame()
        all_file = self.services.fetch_csv_data(url_all, self.username, self.password)
        return all_file, _
