from typing import Dict, Any

from vdb_schema import HelloWorldModel as BaseHelloWorldModel


class HelloWorldModel(BaseHelloWorldModel):
    pass


class HelloWorldRepository:
    @staticmethod
    def generate_example_query() -> Dict[str, Any]:
        """Get data from database and return as dictionary data
        :return: dictionary data
        """
        return {"id": "1", "name": "hello world", "consumer_message": "hello world"}
