from datetime import datetime, timedelta
from app import cfg
from services import MNCCloudServices
from utils import decode_string
from utils import Logging


class RemoveFileRepository:
    def __init__(self):
        self.today_date = datetime.now()
        self.yesterday_date = self.today_date - timedelta(days=7)
        self.delete_date_format = "{}{month:02d}{day:02d}".format(
            self.yesterday_date.year,
            # this format makes month 2 digits (eg: 5 -> 05, 6 -> 06)
            month=self.yesterday_date.month,
            day=self.yesterday_date.day,
        )
        self.username = decode_string(cfg.mnc_cloud_username)
        self.password = decode_string(cfg.mnc_cloud_password)
        self.services = MNCCloudServices.from_base_url(cfg.http_base_url)
        self.mnc_cloud_path_file = cfg.mnc_path_file

    def delete_all_record(self) -> bool:
        """Get url mnc file cloud data and
        delete  record  (created and updated )
        of the last 5th day
        :return: True
        """
        try:
            Logging.info("delete all created and updated file of the last 7th day")
            vdb_url = f"{self.username}/{self.mnc_cloud_path_file}/tmp"
            csv_list = self.services.fetch_list(vdb_url, self.username, self.password)
            filtered_files = [filename for filename in csv_list if  self.delete_date_format in filename]
            for file_name in filtered_files:
                url_created = "{}/{}/tmp/{}".format(
                    self.username,
                    self.mnc_cloud_path_file,
                    file_name)
                self.services.delete_file_after_read(
                    url=url_created,
                    username=self.username,
                    password=self.password,
                )

                Logging.info("successfully {} deleted  file".format(file_name))
            Logging.info("successfully deleted  file from temp of last week data")
        except Exception as e:
            Logging.error("error for result in recommendation method: {}".format(e))

        return True
