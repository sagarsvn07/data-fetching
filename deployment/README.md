
# Kubernetes Deployment

## Deployment

```
kubectl apply -f deploy.yaml
```

## Documentation
please see:
```
https://mnc-repo.mncdigital.com/ai-team/vision_plus/kubernetes-resources
```
