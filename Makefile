install:
	poetry install

update:
	poetry update

pip:
	poetry export --without-hashes --format=requirements.txt > requirements.txt && \
	pip install --upgrade pip && \
	pip install -r requirements.txt && \
	rm -rf requirements.txt

docker:
	docker build --rm \
	-f "Dockerfile" \
	-t vplus/vdb-producer:latest . && \
	docker run --restart always \
	--network db-network \
	--name vdb-producer \
	-e ENV=local \
	-e AMQP_BROKER_URI=pyamqp://guest@local-rabbit// \
	-e TEST_DUMMY=1 \
	-e MYSQL_URI=mysql+pymysql://root:rahasia@local-mysql:3306/inventory \
	-e REDIS_URI=redis://:rahasia@local-redis:6379/0 \
	-d vplus/vdb-producer:latest