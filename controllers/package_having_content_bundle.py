import time
from typing import Dict, Any

import pandas
from celery.exceptions import TimeLimitExceeded, TimeoutError
from ingestor.common.constants import PACKAGE_HAVING_CONTENT_BUNDLE

from app import app as celery, cfg
from configs import AwsConfig
from models import PackageHavingContentBundleRepository, PackageHavingContentBundleModel
from services import S3Database
from utils import Logging, remove_duplicate_created_updated, upsert_array


class PackageHavingContentBundleController(PackageHavingContentBundleRepository):
    s3db: S3Database

    def __init__(self):
        """Define new object controller with db connection object"""
        PackageHavingContentBundleRepository.__init__(self)
        self.having_package_path = AwsConfig().having_package_mapping
        self.file_name = PACKAGE_HAVING_CONTENT_BUNDLE
        PackageHavingContentBundleController.s3db = S3Database.init_db_from_s3(
            file_path="{}/PackageHavingContentBundle.pkl".format(
                self.having_package_path
            ),
            table_name=self.file_name,
        )

    def append_csv(self, data: pandas.DataFrame) -> None:
        """Appending data from s3 with new data
        :return: none
        """
        records = data.to_dict(orient="records")
        PackageHavingContentBundleController.s3db.db.table(
            self.file_name
        ).insert_multiple(records)

    def update_csv(self, data: pandas.DataFrame) -> None:
        """Appending data from s3 with new data
        :return: none
        """
        # get source data
        source_data = PackageHavingContentBundleController.s3db.db.table(
            self.file_name
        ).all()
        # get target data
        target_data = data.to_dict(orient="records")
        # upsert data from source values
        merge_values = upsert_array(
            source_data, target_data, keys=["package_id", "content_bundle_id"]
        )
        # now clear all data in database
        PackageHavingContentBundleController.s3db.db.table(self.file_name).truncate()
        # then reinsert data back to tables
        PackageHavingContentBundleController.s3db.db.table(
            self.file_name
        ).insert_multiple(merge_values)

    @staticmethod
    def send_message_to_consumer(df: pandas.DataFrame) -> Dict[str, Any]:
        """Send message to consumer with chunking data
        :param df: dataframe that already merge between created and updated data
        :return:
        """
        try:
            # send to consumer with chunk value
            n = cfg.http_chunk_data  # chunk row size
            chunks_df = [df[i : i + n] for i in range(0, df.shape[0], n)]
            routing_key = "{}.package_having_content_bundle".format(
                cfg.consumer_routing_name
            )

            for df in chunks_df:
                data = [
                    PackageHavingContentBundleModel.parse_obj(d).json()
                    for d in df.to_dict(orient="records")
                ]

                celery.send_task(
                    routing_key,
                    args=[data],
                    kwargs={},
                    queue=cfg.consumer_queue_name,
                )

                # sleep on 3 seconds
                time.sleep(3)
            return dict(message="success send message: {}".format(routing_key))
        except (TimeoutError, TimeLimitExceeded) as e:
            Logging.error("error for result in recommendation method: {}".format(e))
            return dict(message=str(e))

    def fetch_data(
        self,
    ) -> Dict[str, Any]:
        """This will call model and execute some script, convert ans object model and return value
        :return: dictionary object from consumer side
        """
        created, updated = self.get_package_having_content_bundle()
        if len(created) > 0:
            self.append_csv(created)

        if len(updated) > 0:
            self.update_csv(updated)

        # create dictionary from pandas
        query = remove_duplicate_created_updated(
            created,
            updated,
            ["package_id", "content_bundle_id"],
        )
        if len(query) == 0:
            Logging.info(
                "there's no message to send to producer: {}".format(
                    cfg.consumer_routing_name
                )
            )
            return dict(empty_message=True)

        # send message
        resp = self.send_message_to_consumer(query)
        return resp
