import json
import os
from typing import List, Dict, Any
import pandas as pd
import requests
from joblib import Parallel, delayed
from pandas import DataFrame, concat
from configs import Config
from repository import RedisConnection
from services import S3Services
from utils import Logging
from graphdb import GraphDbConnection, GraphDb



class PosterBanner:

    def __init__(
        self,
    ):
        self._config = Config()
        self._s3_conn = S3Services()
        self._redis_conn = RedisConnection.from_uri(
            self._config.redis_uri_recommendation
        )
        connection = GraphDbConnection.from_uri(
            self._config.graph_conn_uri_writer,
            self._config.graph_conn_uri_reader
        )
        self._graph_conn = GraphDb.from_connection(connection)


    def get_data(self, content_type = 'pay_tv_content' ) -> pd.DataFrame:
        """Get data from offline result module
        :return: Object cluster data
        """
        query = f"""
                g.V().has('{content_type}', 'status', 'Active')
                .project('content_id').by(values('content_id'))
                """
        resp = self._graph_conn.custom_query(query,payload={})
        resp =[rec for record in resp for rec in record]
        resp = DataFrame(resp)
        return resp

    def store_in_redis(
        self,
        records: List[Dict[str, List[Dict[str, Any]]]],
    ) -> bool:
        """Store data prediction in redis
        :param records: object list cluster model
        :return: boolean (true or false)
        """
        for rec in records:
            key = ""
            self._redis_conn.set(
                key,
                json.dumps(rec, default=str),
                ttl=self._config.redis_cache_lifetime,
            )
        return True


    def save_to_s3(self, data):
        try:
            self._s3_conn.write_df_to_pkl_S3(data=data, object_name="mapping/poster_banner.pkl")
            return True
        except Exception as e:
            Logging.info(f"Error {e}")
        finally:
            Logging.info("save_to_s3 executed")



    def filter_content_for_poster_banner(self, content_list):
        cfg = self._config
        token = cfg.content_detail_api_token
        content_detail_api = cfg.content_detail_api
        headers = {"Authorization": token}
        result = {"content_id": [], "image_url": []}
        content_count = len(content_list)
        count = 1
        for val in content_list:
            resp: requests.get
            resp = requests.get(
                content_detail_api + str(val),
                headers=headers,
            )
            if resp.status_code == 200:
                resp = resp.json()
                resp = resp['data']
                if resp['url_square_poster'] not in ["", " ", None] \
                    or resp['url_web_banner_detail'] not in ["", " ", None]:
                    result["content_id"].append(val)
                    if resp['url_web_banner_detail'] not in ["", " ", None]:
                        result[f"image_url"].append(resp['url_web_banner_detail'])
                        Logging.info(f"Found banner for content id: {val} banner: {resp['url_web_banner_detail']}")
                    else:
                        result["image_url"].append(resp['url_square_poster'])
                        Logging.info(f"Found banner for content id: {val} banner: {resp['url_square_poster']}")
            Logging.info(f"Processed {count}/{content_count} content")
            count += 1
        return DataFrame(result)


    def chunkify(self, lst, n):
        return [lst[i::n] for i in range(n)]


    def pay_tv_content_poster_banner(self):
        data = self.get_data('pay_tv_content')
        data = data["content_id"].to_list()
        data = self.chunkify(data, 10)
        result = Parallel(n_jobs=max(1, os.cpu_count() - 1), require="sharedmem")(
            delayed(self.filter_content_for_poster_banner)(content_ids) for content_ids in data
        )
        data = concat(result)
        Logging.info(f"Total available banner for 'pay tv content': {len(data)}")
        if len(data) >= 0:
            self.save_to_s3(data)
            self._redis_conn.set_plain_key("poster_banner:pay_tv", str(data.to_dict('records')))


    def no_pay_tv_content_poster_banner(self):
        data = self.get_data('no_pay_tv_content')
        data = data["content_id"].to_list()
        data = self.chunkify(data, 10)
        result = Parallel(n_jobs=max(1, os.cpu_count() - 1), require="sharedmem")(
            delayed(self.filter_content_for_poster_banner)(content_ids) for content_ids in data
        )
        data = concat(result)
        Logging.info(f"Total available banner for 'no pay tv content': {len(data)}")

        if len(data) >= 0:
            self.save_to_s3(data)
            self._redis_conn.set_plain_key("poster_banner:no_pay_tv", str(data.to_dict('records')))


