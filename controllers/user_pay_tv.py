import time
from typing import Dict, Any

import pandas
from celery.exceptions import TimeLimitExceeded, TimeoutError
from ingestor.common.constants import USER_PAY_TV

from app import app as celery, cfg
from configs import AwsConfig
from models import UserPayTVRepository, UserPayTVModel
from services import S3Database
from utils import Logging, remove_duplicate_created_updated, upsert_array


class UserPayTVController(UserPayTVRepository):
    s3db: S3Database

    def __init__(self):
        """Define new object controller with db connection object"""
        UserPayTVRepository.__init__(self)
        self.having_package_path = AwsConfig().having_package_mapping
        self.file_name = USER_PAY_TV
        UserPayTVController.s3db = S3Database.init_db_from_s3(
            file_path="{}/{}.pkl".format(self.having_package_path, self.file_name),
            table_name=self.file_name,
        )

    def append_csv(self, data: pandas.DataFrame) -> None:
        """Appending data from s3 with new data
        :return: none
        """
        records = data.to_dict(orient="records")
        UserPayTVController.s3db.db.table(self.file_name).insert_multiple(records)

    def upsert_csv(self, data: pandas.DataFrame) -> None:
        """Appending data from s3 with new data
        :return: none
        """
        # get source data
        source_data = UserPayTVController.s3db.db.table(self.file_name).all()
        # get target data
        target_data = data.to_dict(orient="records")
        # upsert data from source values
        merge_values = upsert_array(
            source_data,
            target_data,
            keys=["UserDetail_UDKey"],
        )
        # now clear all data in database
        UserPayTVController.s3db.db.table(self.file_name).truncate()
        # then reinsert data back to tables
        UserPayTVController.s3db.db.table(self.file_name).insert_multiple(merge_values)

    @staticmethod
    def send_message_to_consumer(df: pandas.DataFrame) -> Dict[str, Any]:
        """Send message to consumer with chunking data
        :param df: dataframe that already merge between created and updated data
        :return:
        """
        try:
            # send to consumer with chunk value
            n = cfg.http_chunk_data  # chunk row size
            chunks_df = [df[i : i + n] for i in range(0, df.shape[0], n)]
            routing_key = f"{cfg.consumer_routing_name}.user_pay_tv"

            for df in chunks_df:
                data = [
                    UserPayTVModel.parse_obj(d).json()
                    for d in df.to_dict(orient="records")
                ]

                celery.send_task(
                    routing_key,
                    args=[data],
                    kwargs={},
                    queue=cfg.consumer_queue_name,
                )

                # sleep on 3 seconds
                time.sleep(3)
            return dict(message=f"success send message: {routing_key}")
        except (TimeoutError, TimeLimitExceeded) as e:
            Logging.error(f"error for result in recommendation method: {e}")
            return dict(message=str(e))

    def send_process_to_consumer(self) -> Dict[str, Any]:
        """Send message to consumer with chunking data
        :param df: dataframe that already merge between created and updated data
        :return:
        """
        try:
            routing_key = f"{cfg.consumer_routing_name}.customer"
            time.sleep(3)
            process_file_list = self.features.fetch_list_from_process_file(self.file_name)
            for url in process_file_list:
                process_url = f"{self.username}/{self.mnc_cloud_path_file}/{url}"
                celery.send_task(
                    routing_key,
                    args=[process_url],
                    kwargs={},
                    queue=cfg.consumer_queue_name,
                )
                Logging.info(f'{process_url} send to consumer')

            return dict(message=f"success send message: {routing_key}")
        except (TimeoutError, TimeLimitExceeded) as e:
            Logging.error(f"error on sending message to consumer: {e}")
            return dict(message=str(e))

    def fetch_data(
        self,
    ):
        """This will call hello world model and execute some script, convert ans object model and return value
        :return: dictionary object from consumer side
        """
        created, updated = self.get_user_pay_tv_list()
        if len(created) > 0:
            self.upsert_csv(created)

        if len(updated) > 0:
            self.upsert_csv(updated)


        # create dictionary from pandas
        query = remove_duplicate_created_updated(
            created,
            updated,
            ["paytvprovider_id", "UserDetail_UDKey"],
        )
        if len(query) == 0:
            Logging.info(
                f"there's no message to upload to s3: {cfg.consumer_routing_name}"
            )
            return dict(empty_message=True)

        time.sleep(3)
        self.features.delete_all_csv_data(self.file_name)

