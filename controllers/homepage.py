import time
from typing import Dict, Any

import pandas
from celery.exceptions import TimeLimitExceeded, TimeoutError
from ingestor.common import HOMEPAGE

from app import app as celery, cfg
from models import HomePageRepository, HomePageModel
from services import S3Database
from utils import Logging, remove_duplicate_created_updated, upsert_array


class HomePageController(HomePageRepository):
    s3db: S3Database

    def __init__(self):
        """Define new object controller with db connection object"""
        HomePageRepository.__init__(self)
        self.file_name = HOMEPAGE
        HomePageController.s3db = S3Database.init_db_from_s3(
            file_path="vdb/{}.pkl".format(self.file_name),
            table_name=self.file_name,
        )

    def upsert_csv(self, data: pandas.DataFrame) -> None:
        """Appending data from s3 with new data
        :return: none
        """
        # get source data
        source_data = HomePageController.s3db.db.table(self.file_name).all()
        # get target data
        target_data = data.to_dict(orient="records")
        # upsert data from source values
        merge_values = upsert_array(source_data, target_data, keys=["homepage_id"])
        # now clear all data in database
        HomePageController.s3db.db.table(self.file_name).truncate()
        # then reinsert data back to tables
        HomePageController.s3db.db.table(self.file_name).insert_multiple(merge_values)


    def send_message_to_consumer(self,df: pandas.DataFrame) -> Dict[str, Any]:
        """Send message to consumer with chunking data
        :param df: dataframe that already merge between created and updated data
        :return:
        """
        try:
            # send to consumer with chunk value
            n = cfg.http_chunk_data  # chunk row size
            chunks_df = [df[i : i + n] for i in range(0, df.shape[0], n)]
            routing_key = "{}.homepage".format(cfg.consumer_routing_name)
            counter = 1
            for df in chunks_df:
                # data = [
                #     HomePageModel.parse_obj(d).json()
                #     for d in df.to_dict(orient="records")
                # ]
                process_url = self.features.move_data_to_be_process(counter, df, self.file_name)
                celery.send_task(
                    routing_key,
                    args=[process_url],
                    kwargs={},
                    queue=cfg.consumer_queue_name,
                )
                Logging.info(f'{process_url} send to consumer and move to process url')
                counter = counter + 1
            time.sleep(3)
            self.features.delete_all_csv_data(self.file_name)
            return dict(message="success send message: {}".format(routing_key))
        except (TimeoutError, TimeLimitExceeded) as e:
            Logging.error("error for result in recommendation method: {}".format(e))
            return dict(message=str(e))


    def send_process_to_consumer(self) -> Dict[str, Any]:
        """Send message to consumer with chunking data
        :param df: dataframe that already merge between created and updated data
        :return:
        """
        try:
            routing_key = "{}.homepage".format(cfg.consumer_routing_name)
            process_file_list = self.features.fetch_list_from_process_file(self.file_name)
            for url in process_file_list:
                process_url = f"{self.username}/{self.mnc_cloud_path_file}/{self.file_name}/{url}"
                celery.send_task(
                    routing_key,
                    args=[process_url],
                    kwargs={},
                    queue=cfg.consumer_queue_name,
                )
                Logging.info(f'{process_url} send to consumer')

            return dict(message="success send message: {}".format(routing_key))
        except (TimeoutError, TimeLimitExceeded) as e:
            Logging.error("error on sending message to consumer: {}".format(e))
            return dict(message=str(e))


    def fetch_data(
        self,
    ) -> Dict[str, Any]:
        """This will call hello world model and execute some script, convert ans object model and return value
        :return: dictionary object from consumer side
        """
        created, updated = self.get_homepage_list()
        time.sleep(1)
        if len(created) > 0:
            self.upsert_csv(created)

        if len(updated) > 0:
            self.upsert_csv(updated)
        # create dictionary from pandas
        query = remove_duplicate_created_updated(created, updated, ["homepage_id"])
        if len(query) == 0:
            # check in process url
            time.sleep(3)
            self.send_process_to_consumer()
            Logging.info(
                "there's no message availble to send to producer: {}".format(
                    cfg.consumer_routing_name
                )
            )
            return dict(empty_message=True)

        # send message
        resp = self.send_message_to_consumer(query)
        return resp



