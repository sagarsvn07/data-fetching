from typing import Dict, Any

from celery.exceptions import TimeLimitExceeded, TimeoutError

from app import app as celery, cfg
from models import HelloWorldRepository, HelloWorldModel
from utils import Logging


class HelloWorldController(HelloWorldRepository):
    def __init__(self):
        """Define new object controller with db connection object"""
        HelloWorldRepository.__init__(self)

    def send_message(
        self,
    ) -> Dict[str, Any]:
        """This will call hello world model and execute some script, convert ans object model and return value
        :return: dictionary object from consumer side
        """
        try:
            query = self.generate_example_query()
            if len(query) == 0:
                Logging.info(
                    "there's no message to send to producer: {}".format(
                        cfg.consumer_routing_name
                    )
                )
                return dict(empty_message=True)

            data = HelloWorldModel(**query).json()
            routing_key = "{}.hello_world".format(cfg.consumer_routing_name)

            celery.send_task(
                routing_key,
                args=[data],
                kwargs={},
                queue=cfg.consumer_queue_name,
            )
            return dict(message="success send message: {}".format(routing_key))
        except (TimeoutError, TimeLimitExceeded) as e:
            Logging.error("error for result in recommendation method: {}".format(e))
            return dict(message=str(e))
