import time
from typing import Dict, Any

import pandas
from celery.exceptions import TimeLimitExceeded, TimeoutError
from ingestor.common.constants import CONTENT_HAVING_COUNTRY

from app import app as celery, cfg
from configs import AwsConfig
from models import ContentHavingCountryRepository, ContentHavingCountryModel
from services import S3Database
from utils import Logging, remove_duplicate_created_updated, upsert_array



class ContentHavingCountryController(ContentHavingCountryRepository):
    s3db: S3Database

    def __init__(self):
        """Define new object controller with db connection object"""
        ContentHavingCountryRepository.__init__(self)
        self.file_name = CONTENT_HAVING_COUNTRY
        self.having_package_path = AwsConfig().having_package_mapping
        ContentHavingCountryController.s3db = S3Database.init_db_from_s3(
            file_path="{}/{}.pkl".format(self.having_package_path, self.file_name),
            table_name=self.file_name,
        )

    def append_csv(self, data: pandas.DataFrame) -> None:
        """Appending data from s3 with new data
        :return: none
        """
        records = data.to_dict(orient="records")
        # now clear all data in database
        ContentHavingCountryController.s3db.db.table(self.file_name).truncate()
        # then reinsert data back to tables
        ContentHavingCountryController.s3db.db.table(self.file_name).insert_multiple(
            records
        )

    def upsert_csv(self, data: pandas.DataFrame) -> None:
        """Appending data from s3 with new data
        :return: none
        """
        # get source data
        source_data = ContentHavingCountryController.s3db.db.table(self.file_name).all()
        # get target data
        target_data = data.to_dict(orient="records")
        # upsert data from source values
        merge_values = upsert_array(
            source_data, target_data, keys=["content_id", "country_id"]
        )
        # now clear all data in database
        ContentHavingCountryController.s3db.db.table(self.file_name).truncate()
        # then reinsert data back to tables
        ContentHavingCountryController.s3db.db.table(self.file_name).insert_multiple(
            merge_values
        )

    def send_message_to_consumer(self, df: pandas.DataFrame) -> Dict[str, Any]:
        """Send message to consumer with chunking data

        :param df: DataFrame that has already merged created and updated data
        :return: Dictionary with message result
        """
        try:
            routing_key = f"{cfg.consumer_routing_name}.content_having_country"
            time.sleep(3)
            process_url = self.features.move_data_to_be_process(
                file_name=self.file_name,
                created_data=df,
                mapping_file=True,
                counter=0
            )
            celery.send_task(
                routing_key,
                args=[process_url],
                kwargs={},
                queue=cfg.consumer_queue_name
            )

            # Sleep for 3 seconds
            time.sleep(3)
            self.features.delete_all_csv_data(self.file_name)
            return {"message": f"Successfully sent message: {routing_key}"}
        except (TimeoutError, TimeLimitExceeded) as e:
            Logging.error(f"Error in recommendation method: {e}")
            return {"message": str(e)}

    def send_process_to_consumer(self) -> Dict[str, Any]:
        """Send message to consumer with process url
        :return:True
        """
        try:
            routing_key = f"{cfg.consumer_routing_name}.content_having_country"
            time.sleep(3)
            process_file_list = self.features.fetch_list_from_process_file(self.file_name)
            if process_file_list:
                process_url = f"{self.username}/{self.mnc_cloud_path_file}/{self.file_name}/{process_file_list[0]}"
                celery.send_task(
                    routing_key,
                    args=[process_url],
                    kwargs={},
                    queue=cfg.consumer_queue_name,
                )
                Logging.info(f'{process_url} send to consumer')
                return dict(message="success send message: {}".format(routing_key))
        except (TimeoutError, TimeLimitExceeded) as e:
            Logging.error("error on sending message to consumer: {}".format(e))
            return dict(message=str(e))

    def fetch_data(
        self,
    ) -> Dict[str, Any]:
        """This will call model and execute some script, convert ans object model and return value
        :return: dictionary object from consumer side
        """
        created = self.get_content_having_country_list()
        if len(created) > 0:
            self.append_csv(created)

        if len(created) == 0:
            self.send_process_to_consumer()
            Logging.info(
                "there's no message available to send to producer: {}".format(
                    cfg.consumer_routing_name
                )
            )
            return dict(empty_message=True)
        # send message
        resp = self.send_message_to_consumer(created)
        return resp
