from models import RemoveFileRepository


class RemoveFileController(RemoveFileRepository):

    def remove_file(self):
        """
         delete file present
         in MNC Cloud
        :return: True
        """
        self.delete_all_record()
