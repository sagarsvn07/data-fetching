pymongo.py# SonarQube Example with Code Smell

class DataProcessor:
    def process_data(self, data):
        for item in data:
            if item % 2 == 0:
                print(f"Even: {item}")
            else:
                print(f"Odd: {item}")

# Main Code
if __name__ == "__main__":
    data = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    processor = DataProcessor()
    processor.process_data(data)
